import { checkIfImageExists, q, makeRequest, singleGame } from "./helpers.js";

document.addEventListener('DOMContentLoaded', () => {
    console.log('loaded')
});

(async () => {
    try {
        const response = await makeRequest('https://www.ct-interactive.dev/samples/');
        const sorted = response.games_list.sort((a, b) => +a.order - +  b.order);
        const app = q('#app');
        sorted.map((game) => {
            checkIfImageExists(game.icon, (exists) => {
                if (exists) {
                    app.innerHTML += singleGame(game);
                    if (game?.bigIco === 1) {
                        q('.single-game').classList.remove('game-image');
                        q('.single-game').classList.remove('image-container');
                        q('.single-game').classList.add('big-image');
                        q('.single-game').classList.add('big-image-container');
                    }
                }
            });
        });
        q('#app').addEventListener('click', (e) => {
            e.target.nextElementSibling.click();
        });
    } catch (err) {
        console.log(err);
    }
})();

