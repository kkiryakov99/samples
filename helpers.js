export const checkIfImageExists = (url, callback) => {
    const img = new Image();
    img.src = url;

    if (img.complete) {
        callback(true);
    } else {
        img.onload = () => {
            callback(true);
        };

        img.onerror = () => {
            callback(false);
        };
    }
}

export const makeRequest = (url) => {
    return new Promise((resolve, reject) => {
        const xhr = new XMLHttpRequest();
        xhr.open('GET', url, true);

        xhr.onload = () => {
            if (xhr.status === 200) {
                resolve(JSON.parse(xhr.response));
            } else {
                reject(new Error('Request failed. Error code: ' + xhr.status));
            }
        };

        xhr.onerror = () => {
            reject(new Error('Request failed.'));
        };

        xhr.send();
    });
}

export const singleGame = (game) => {
    return `<div class="single-game">
    <div class="image-container" >
         <img src=${game.icon} title=${game.name} class="game-image" />
         <a id="link" href="${game.link}" style="display: none" target="_blank" >link</a>
    </div>
  </div>
`;
}
// 
export const q = (selector) => document.querySelector(selector);